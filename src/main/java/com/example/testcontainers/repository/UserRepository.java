package com.example.testcontainers.repository;

import com.example.testcontainers.repository.entity.User;

public interface UserRepository {
    User getTriggerParamById(int triggerId);
    void addUser(User user);
}
