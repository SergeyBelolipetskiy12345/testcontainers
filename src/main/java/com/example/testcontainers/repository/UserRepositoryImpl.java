package com.example.testcontainers.repository;

import com.example.testcontainers.repository.entity.User;
import java.util.Map;
import liquibase.pro.packaged.I;
import lombok.RequiredArgsConstructor;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class UserRepositoryImpl implements UserRepository{
    private final NamedParameterJdbcTemplate jdbcTemplate;

    private static final String GET_USERS_BY_ID = "SELECT *" +
            " FROM users" +
            " WHERE users.id = :id";

    private static final String INSERT_USER =
        "INSERT INTO users (name, age) VALUES (:name, :age)";

    @Override
    public User getTriggerParamById(int triggerId) {
        return jdbcTemplate.queryForObject(GET_USERS_BY_ID,
            new MapSqlParameterSource("id", triggerId),
            new BeanPropertyRowMapper<>(User.class));
    }

    @Override
    public void addUser(User user){
        jdbcTemplate.update(INSERT_USER, new MapSqlParameterSource()
            .addValue("name", user.getName())
            .addValue("age", user.getAge()));
    }
}
