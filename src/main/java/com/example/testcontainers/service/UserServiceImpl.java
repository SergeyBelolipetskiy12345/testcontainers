package com.example.testcontainers.service;

import com.example.testcontainers.repository.UserRepository;
import com.example.testcontainers.repository.entity.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService{
    private final UserRepository userRepository;

    @Override
    public User getUserById(int id){
        return userRepository.getTriggerParamById(id);
    }

    @Override
    public void addUser(User user){
        userRepository.addUser(user);
    }
}
