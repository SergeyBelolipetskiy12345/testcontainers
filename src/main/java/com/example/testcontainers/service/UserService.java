package com.example.testcontainers.service;

import com.example.testcontainers.repository.entity.User;

public interface UserService {
    User getUserById(int id);
    void addUser(User user);
}
