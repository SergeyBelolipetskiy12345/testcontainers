package com.example.testcontainers.config;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Endpoints {
    BASE_URI_LOCAL("http://localhost"),
    BASE_PATH("/pl-testcontainers"),
    GET_USERS_BY_ID("/user");

    private final String url;
}
