package com.example.testcontainers.hints;

import com.example.testcontainers.kafka.SendService;
import com.example.testcontainers.repository.entity.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.*;

import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.jupiter.api.Test;

import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import org.testcontainers.utility.DockerImageName;

import java.time.Duration;
import java.util.List;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Testcontainers
public class Kafka2Test {

    @Container
    KafkaContainer kafkaContainer = new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:6.2.1"));

    private final ObjectMapper mapper = new ObjectMapper();

    @Test
    void setMessage() throws JsonProcessingException {
        String bootstrapServers = kafkaContainer.getBootstrapServers();
        String topicName = "test";
        SendService sendService = new SendService(bootstrapServers, topicName);
        sendService.sendRecords(List.of(mapper.writeValueAsString(new User(1, "Sergey", 31))));

        Properties properties = new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, "test");
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        Consumer<String, String> consumer = new KafkaConsumer<>(properties);
        consumer.subscribe(List.of(topicName));
        ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(10000));
        consumer.close();

        records.forEach(r -> {
            User user;
            try {
                 user = mapper.readValue(r.value(), User.class);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
            assertEquals(user.getName(), "Sergey");
            assertEquals(user.getId(), 1);
            assertEquals(user.getAge(), 31);
        });

    }

}
