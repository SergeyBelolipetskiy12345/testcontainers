package com.example.testcontainers.hints;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.example.testcontainers.repository.entity.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.builder.RequestSpecBuilder;

import org.junit.jupiter.api.Test;
import org.mockserver.client.MockServerClient;
import org.mockserver.model.MediaType;
import org.springframework.http.HttpHeaders;

import org.testcontainers.containers.MockServerContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;
@Testcontainers
public class MockServer2Test {

  private final ObjectMapper mapper = new ObjectMapper();

  @Container
  MockServerContainer mockServer =
      new MockServerContainer(DockerImageName.parse("mockserver/mockserver"));

  @Test
  public void testMockServer() throws Exception {

    User user = new User(1, "Николай", 25);

    try (MockServerClient client = new MockServerClient(mockServer.getHost(),
        mockServer.getServerPort())) {
      assertThat(client.hasStarted()).as("Сервер стартовал").isTrue();

      client.when(request().withPath("/test"))
          .respond(response().withBody(mapper.writeValueAsString(user)).withContentType(
              MediaType.parse(APPLICATION_JSON_VALUE)));

      User userFromMockServer = given().spec(new RequestSpecBuilder()
              .setBaseUri(mockServer.getEndpoint())
              .setBasePath("/test")
              .setPort(mockServer.getServerPort())
              .build())
          .header(HttpHeaders.CONTENT_TYPE, APPLICATION_JSON_VALUE)
          .get().as(User.class);

      assertEquals(user, userFromMockServer);

    }
  }

}
