package com.example.testcontainers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockserver.model.HttpRequest.request;

import org.junit.jupiter.api.Test;
import org.mockserver.client.MockServerClient;
import org.mockserver.model.HttpResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.testcontainers.containers.MockServerContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

@Testcontainers
public class MockServerTest {

  WebClient webClient;

  @Container
  MockServerContainer container =
      new MockServerContainer(DockerImageName.parse("mockserver/mockserver"));


  @Test
  void testMockServer(){
    try (MockServerClient client = new MockServerClient(container.getHost(),
        container.getServerPort())) {

      client.when(request().withPath("/test")).respond(HttpResponse.response().withBody("privet"));

      webClient = WebClient.builder().baseUrl(container.getEndpoint()).build();

      String answer = webClient.get().uri(u -> u.host(container.getHost())
          .port(container.getServerPort()).path("/test").build())
          .retrieve().bodyToMono(String.class).block();

      assertEquals(answer, "privet");
    }
  }
}
































//  webClient = WebClient.builder().baseUrl(container.getEndpoint()).build();
//  String answer = webClient.get().uri(u -> u.path("/test")
//      .host(container.getHost()).port(container.getServerPort())
//      .build()).retrieve().bodyToMono(String.class).block();
