package com.example.testcontainers;

import com.example.testcontainers.config.ContainerInit;
import com.example.testcontainers.config.Endpoints;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;

import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@Import(ContainerInit.class)
public class BaseTest {
    @LocalServerPort
    public int port;

    @BeforeEach
    protected void setUp() {
        getRequestSpecification();
    }

    protected RequestSpecification getRequestSpecification() {
        return new RequestSpecBuilder()
            .setBaseUri(Endpoints.BASE_URI_LOCAL.getUrl())
            .setBasePath(Endpoints.BASE_PATH.getUrl())
            .setPort(port)
            .build().log().all();
    }

}
