package com.example.testcontainers.kafka;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.Duration;
import java.util.List;
import java.util.Properties;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.KafkaContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

@Testcontainers
public class KafkaTest {

  @Container
  KafkaContainer container =
      new KafkaContainer(DockerImageName.parse("confluentinc/cp-kafka:6.2.1"));

  @Test
  void testKafka(){
    String topic = "test";
    String bootstrapServer = container.getBootstrapServers();

    SendService sendService = new SendService(bootstrapServer, topic);
    sendService.sendRecords(List.of("privet"));

    Properties properties = new Properties();
    properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServer);
    properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
    properties.put(ConsumerConfig.GROUP_ID_CONFIG, "test");
    properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

    Consumer<String, String> consumer = new KafkaConsumer<>(properties);
    consumer.subscribe(List.of(topic));
    ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(10000));
    consumer.close();

    records.forEach(r -> {
      assertEquals(r.value(), "privet");
    });
  }

}
