package com.example.testcontainers.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.test.context.TestConfiguration;
import org.testcontainers.containers.PostgreSQLContainer;

@TestConfiguration
@Slf4j
public class ContainerInit {
    private static final String PG_DOCKER_IMAGE = "postgres:14.2-alpine3.15";
    private static final String INIT_SCRIPT_PATH = "db/initial/initial.sql";
  @SuppressWarnings("resource")
    protected static PostgreSQLContainer<?> PG_CONTAINER =
      new PostgreSQLContainer<>(PG_DOCKER_IMAGE)
            .withInitScript(INIT_SCRIPT_PATH);

    static {
        PG_CONTAINER.start();

        System.setProperty("spring.datasource.url", PG_CONTAINER.getJdbcUrl());
        System.setProperty("spring.datasource.username", PG_CONTAINER.getUsername());
        System.setProperty("spring.datasource.password", PG_CONTAINER.getPassword());
    }

}
