package com.example.testcontainers.controller;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

import com.example.testcontainers.BaseTest;
import com.example.testcontainers.config.ContainerInit;
import com.example.testcontainers.config.Endpoints;
import com.example.testcontainers.repository.entity.User;
import io.restassured.builder.RequestSpecBuilder;
import org.junit.jupiter.api.Test;
import org.junit.runner.Request;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

public class UserControllerTest extends BaseTest{

  @LocalServerPort
  private int port;

  @Test
  @Sql("/add-users.sql")
  void testGetUserById(){
      User user = given().spec(new RequestSpecBuilder()
          .setBaseUri(Endpoints.BASE_URI_LOCAL.getUrl())
          .setBasePath(Endpoints.BASE_PATH.getUrl())
          .setPort(port)
          .build()).get(Endpoints.GET_USERS_BY_ID.getUrl() + "/" + 1)
          .as(User.class);

      assertEquals(user.getName(), "Alex");

  }

}
